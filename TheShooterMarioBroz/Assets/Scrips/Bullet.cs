﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 20f;
    public Rigidbody2D rb;
    private string enemyToDestroy;


    // Start is called before the first frame update
    void Start()
    {
        rb.velocity = transform.right * speed;
    }

    void OnTriggerEnter2D (Collider2D hitInfo)
    {
        Debug.Log(hitInfo.tag);
        enemyToDestroy = (hitInfo.name);
        

        if (hitInfo.gameObject.tag == "enemy")
        {
            Destroy(GameObject.Find(enemyToDestroy));
        }

        Destroy(gameObject);
    }
       

}
