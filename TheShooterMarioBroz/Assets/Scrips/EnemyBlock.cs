﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBlock : MonoBehaviour
{
    public enum tipoComportamientoBlock { pasivo, persecucion}

    public tipoComportamientoBlock comportamiento = tipoComportamientoBlock.pasivo;

    public float entradaZonaPersecucion = 10f;
    public float salidaZonaPersecucion = 20f;

    public Transform Player;

    int direccion = 1;
    public float velocidad = 10f;
     float quieto = 0f;

    Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    
    void Update()
    {
        float dist = Vector3.Distance(Player.position, transform.position);
//        print("Distance to other: " + dist);

        switch (comportamiento)
        {
            case tipoComportamientoBlock.pasivo:

                rb.velocity = new Vector2(quieto * direccion, rb.velocity.y);

                if(dist < entradaZonaPersecucion)
                {
                    comportamiento = tipoComportamientoBlock.persecucion;
                }

                break;

            case tipoComportamientoBlock.persecucion:

                rb.velocity = new Vector2(velocidad * direccion, rb.velocity.y);

                if (Player.position.x > transform.position.x) direccion = 1;
                if (Player.position.x < transform.position.x) direccion = -1;

                if (dist > entradaZonaPersecucion)
                {
                    comportamiento = tipoComportamientoBlock.pasivo;
                }

                break;
        } 
    }
}
