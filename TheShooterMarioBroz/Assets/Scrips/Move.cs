﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Move : MonoBehaviour
{
    public int playerSpeed = 10;
    private bool facingRight = false;
    public int playerJumpPower = 1250;
    private float moveX;
    private float moveY;
    public bool isGrounded;
    public ParticleSystem dust;

    public Transform spawnpoint;

    public Animator animator;

    void Update()
    {
        PlayerMove();
    }

    void PlayerMove()
    {
        animator.SetFloat("Speed", Mathf.Abs(moveX));

        moveX = Input.GetAxis("Horizontal");
        if (Input.GetButtonDown("Jump") && isGrounded == true)
        {
            Jump();
            animator.SetBool("IsJumping", true);
        }

        if (moveX < 0.0f && facingRight == false)
        {
            FlipPlayer();
        }
        else if (moveX > 0.0f && facingRight == true)
        {
            FlipPlayer();
        }
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(moveX * playerSpeed, gameObject.GetComponent<Rigidbody2D>().velocity.y);

    }

    

    void Jump()
    {
        GetComponent<Rigidbody2D>().AddForce(Vector2.up * playerJumpPower);
        isGrounded = false;
        
    }

    void FlipPlayer()
    {
        facingRight = !facingRight;

        transform.Rotate(0f, 180f, 0f);
        /*
        Vector2 localScale = gameObject.transform.localScale;
        localScale.x *= -1;
        transform.localScale = localScale;
        */
    }

    void OnCollisionEnter2D (Collision2D col)
    {
        
        if (col.gameObject.tag == "ground")
        {
            isGrounded = true;
            animator.SetBool("IsJumping", false);
        }
        if (col.gameObject.name == "Finish Zone")
        {
            SceneManager.LoadScene("Win");
        }
        if (col.gameObject.tag == "enemy")
        {
            Respawn();
            //SceneManager.LoadScene("Game Over");
        }
    }

    public void Respawn()
    {
        this.transform.position = spawnpoint.position;
    }

    void DustPlay()
    {
        dust.Play();
    }

    void DustStop()
    {
        dust.Stop();
    }
}
