﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Dead : MonoBehaviour
{
    public Transform spawnpoint;

    void Update()
    {
        if (gameObject.transform.position.y < -6)
        {
            Respawn();
        }
    }
    void Die()
    {
        SceneManager.LoadScene("Game Over");
    }

    public void Respawn()
    {
        this.transform.position = spawnpoint.position;
    }
}
