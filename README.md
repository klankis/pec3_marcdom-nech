He decidido utilizar el juego de la PEC2, por 2 motivos, mejorar el juego y tener todo lo aprendido en uno. 

El "Héroe" tiene 3 animaciones: Idle, Run, Jump.
El sistema de partículas que he creado es de tierra que se levanta al correr.
La IA ha sido lo ultimo que he implementado, he encontrado muchos vídeos donde utilizan esta IA mas avanzada:
https://arongranberg.com/astar/
Pero he decidido hacer una desde 0 ya que no sabia si se podía aplicar Scrips externos.
Los enemigos que tienen IA son los bloques con pinchos, estos están quietos y te persiguen (en horizontal) si te acercas hacia ellos obstaculizandote el paso.
El Héroe puede disparar usando la tecla "left Control", las balas que se generan se destruyen al colisionar y destruye el GameObject si este tiene el TAG de "enemy".
Ahora siempre que el jugador muera reaparece al inicio del juego. No he sabido hacer que los enemigos eliminados reaparecieran al morir.
El uso de TAGs ya lo había hecho en la PEC2.


link del video:

https://youtu.be/xR-Ryu_y8tg

TAG GitLab:

https://gitlab.com/klankis/pec3_marcdom-nech